/*
 *  Copyright 2017 Ginnungagap Informatik AB
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package se.g11.sanctions;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by rickard on 2017-05-22.
 */
public class Search {

    static public TopDocs query(IndexContainer indexContainer, String query) throws IOException, ParseException {
        Query q=parse(indexContainer.queryParser,query);

        return null;
    }

    static public Query parse(QueryParser qp,String query) throws IOException, org.apache.lucene.queryparser.classic.ParseException {
        qp.setLowercaseExpandedTerms(false);
        return qp.parse(query);
    }

    static public IndexSearcher load(Class anchor) {
        URL url = anchor.getClassLoader().getResource("index.zip");

        Path path = null;
        try {
            path = Paths.get(url.toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }

        FileSystem zipfs = null;
        try {
            zipfs = FileSystems.newFileSystem(path, anchor.getClassLoader());
            Path indexPath = zipfs.getPath("/");

            // Read index from files system to lucene RAM based directory.
            FSDirectory dir = new NIOFSDirectory(indexPath, FSLockFactory.getDefault());
            Directory directory = new RAMDirectory(dir, IOContext.DEFAULT);
            IndexReader reader = DirectoryReader.open(directory);
            return new IndexSearcher(reader);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}
